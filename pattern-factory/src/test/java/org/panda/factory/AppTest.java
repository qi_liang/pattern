package org.panda.factory;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.panda.factory.framework.Factory;
import org.panda.factory.idcard.IDCardFactory;
import org.panda.factory.idcard.Product;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    @Test
    public void test(){
        Factory factory = new IDCardFactory();
        Product card1 = factory.create("小明");
        Product card2 = factory.create("小红");
        Product card3 = factory.create("小刚");
        card1.use();
        card2.use();
        card3.use();
    }
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }
}
