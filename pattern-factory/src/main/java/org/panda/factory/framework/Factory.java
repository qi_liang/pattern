package org.panda.factory.framework;

import org.panda.factory.idcard.Product;

/**
 *  抽象工厂类
 *  @author qi
 */
public abstract class Factory {

    public final Product create(String owner){
        Product p = createProduct(owner);
        registerProduct(p);
        return p;
    }

    /**
     *  生产产品
     * @param owner
     * @return
     */
    protected abstract Product createProduct(String owner);

    /**
     *  注册产品
     * @param product
     */
    protected abstract void registerProduct(Product product);
}
