package org.panda.factory.idcard;

/**
 * 抽象产品类
 * @author qi
 */
public interface Product {
    /**
     * 使用
     */
    void use();
}
