package org.panda.factory.idcard;

/**
 * ID卡产品
 * @author qi
 */
public class IDCard implements Product {

    private String owner;

    public IDCard(String owner) {
        System.out.println("制作IDCard："+owner);
        this.owner = owner;
    }

    @Override
    public void use() {
        System.out.println("使用IDCard："+owner);
    }

    public String getOwner(){

        return this.owner;
    }
}
