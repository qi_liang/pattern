package org.panda.factory.idcard;

import org.panda.factory.framework.Factory;

import java.util.HashMap;
import java.util.Map;

/**
 * 身份证工厂
 * @author qi
 */
public class IDCardFactory extends Factory {

 //   private List<String> idCards = new ArrayList<>();

   private Map<String,Product> idCards = new HashMap<>();

    @Override
    protected Product createProduct(String owner) {
        return new IDCard(owner);
    }

    @Override
    protected void registerProduct(Product product) {
        IDCard idCard = (IDCard) product;
        idCards.put(idCard.getOwner(),idCard);
    }

    public Product getIdCard(String owner) {

        return idCards.get(owner);
    }
}
