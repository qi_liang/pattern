package org.panda.decorator;

/**
 * @Author: qi
 * @Description:
 * @Date: Create in 12:05 AM 2019/6/9
 */
public class StringDisplay extends Display {
    /**
     * 要显示的字符串
     */
    private String str;

    /**
     * 通过参数传入要显示的字符串
     * @param str
     */
    public StringDisplay(String str) {
        this.str = str;
    }

    /**
     * 字符数
     * @return
     */
    @Override
    public int getColumns() {
        return str.getBytes().length;
    }

    /**
     *  行数1
     * @return
     */
    @Override
    public int getRows() {
        return 1;
    }

    /**
     * 仅当row为0时返回值
     * @param row
     * @return
     */
    @Override
    public String getRowText(int row) {
        if (row==0){
            return str;
        }else {
            return null;
        }
    }
}
