package org.panda.decorator;

/**
 * @Author: qi
 * @Description:
 * @Date: Create in 12:09 AM 2019/6/9
 */
public abstract class Border extends Display {
    /**
     * 表示被装饰物
     */
    protected Display display;
    /**
     * 在生成实例时通过参数指定被装饰物
     * @param display
     */
    protected Border(Display display) {
        this.display = display;
    }

}
