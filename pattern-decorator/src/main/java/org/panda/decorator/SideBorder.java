package org.panda.decorator;

/**
 * @Author: qi
 * @Description:
 * @Date: Create in 12:12 AM 2019/6/9
 */
public class SideBorder extends Border {

    /**
     *  表示装饰边框的字符
     */
    private char borderChar;
    /**
     * 在生成实例时通过参数指定被装饰物
     *
     * @param display
     */
    protected SideBorder(Display display,char ch) {
        super(display);
        this.borderChar = ch;
    }

    /**
     * 字符数为字符串字符数加上两侧的边框字符数
     * @return
     */
    @Override
    public int getColumns() {
        return 1+display.getColumns()+1;
    }

    /**
     * 行数即被装饰物的行数
     * @return
     */
    @Override
    public int getRows() {
        return display.getRows();
    }

    @Override
    public String getRowText(int row) {

        return borderChar+display.getRowText(row)+borderChar;
    }

}
