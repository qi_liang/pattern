package org.panda.flyweight;

import java.util.HashMap;

/**
 * 字节工厂
 * @author qi
 */
public class BigCharFactory {

    private HashMap pool = new HashMap();
    /**
     * 字节工厂-单例
     */
    private static BigCharFactory singleton = new BigCharFactory();

    private BigCharFactory(){
    }

    public static BigCharFactory getInstance(){

        return singleton;
    }

    public synchronized BigChar getBigchar(Character charName){
        BigChar bigChar = (BigChar) pool.get(charName);
        if (bigChar==null){
            bigChar = new BigChar(charName);
            pool.put(charName,bigChar);
        }
        return bigChar;
    }
}
