package org.panda.proxy;

public class PrinterProxy implements Printable{

    private String name;

    private Printer printer;

    public PrinterProxy() {
    }

    public PrinterProxy(String name) {
        this.name = name;
    }

    @Override
    public synchronized void setPrintName(String name) {
        if (printer!=null){
            printer.setPrintName(name);
        }
        this.name = name;
    }

    @Override
    public String getPrinterName() {
        return this.name;
    }

    @Override
    public void print(String string) {
        realize();
        printer.print(string);
    }

    private synchronized void realize(){
        if (printer==null){
            printer = new Printer(name);
        }
    }
}
