package org.panda.memento;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class Gamer {

    private int money;

    private List<String> fruits = new ArrayList<>();

    private Random random = new Random();

    private static String[] fruitName = {"苹果","葡萄","香蕉","橘子"};

    public Gamer(int money){
        this.money = money;
    }
    public int getMoney(){
        return money;
    }

    //骰子进行游戏
    public void bet(){
        int dice = random.nextInt(6)+1;
        if (dice ==1){
            money+=100;
            System.out.println("所持金钱增加了。");
        }else if(dice == 2){
            money /=2;
            System.out.println("所持金钱减半了。");
        }else if (dice == 6){
            String f = getFrult();
            System.out.println("获得了水果{"+f+"}");
            fruits.add(f);
        }else {
            System.out.println("什么都没有发生。");
        }
    }

    /**
     *  创建快照
     */
    public Memento createMemento(){
        Memento memento = new Memento(money);
        Iterator<String> it = fruits.iterator();
        while (it.hasNext()){
            String f = it.next();
            //保存好吃的水果
            if (f.startsWith("好吃")){
                memento.addFruit(f);
            }
        }
        return memento;
    }

    /**
     * 撤销
     */
    public void restoreMemento(Memento memento){
        this.money = memento.money;
        this.fruits = memento.getFruits();
    }

    public String getFrult(){
        String prefix = "";
        if (random.nextBoolean()){
            prefix = "好吃的";
        }
        return  prefix+fruitName[random.nextInt(fruitName.length)];
    }

    @Override
    public String toString() {
        return "Gamer{" +
                "money=" + money +
                ", fruits=" + fruits +
                '}';
    }
}
