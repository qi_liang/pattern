package org.panda.memento;

import java.util.ArrayList;
import java.util.List;

/**
 * 快照
 * @author qi
 */
public class Memento {
    int money;
    ArrayList<String> fruits ;
    public int getMoney(){
        return money;
    }

    Memento(int money){
        this.money = money;
        this.fruits = new ArrayList<>();
    }

    void addFruit(String fruit){
        fruits.add(fruit);
    }

    List<String> getFruits(){
        return (List<String>) fruits.clone();
    }

}
