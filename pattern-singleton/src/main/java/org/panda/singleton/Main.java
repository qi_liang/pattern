package org.panda.singleton;

/**
 * @Author: qi
 * @Description:
 * @Date: Create in 6:16 AM 2019/5/21
 */
public class Main {

    public static void main(String[] args) {
       Singleton obj1 = Singleton.getSingleton();
       Singleton obj2 = Singleton.getSingleton();

       if (obj1 == obj2){
           System.out.println("obj1 与 obj2 同一实例");
       }else {
           System.out.println("obj1 与 obj2 不是同一实例");
       }
    }
}
