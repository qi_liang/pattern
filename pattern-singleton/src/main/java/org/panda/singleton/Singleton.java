package org.panda.singleton;

/**
 * @Author: qi
 * @Description: 单例模式
 * @Date: Create in 6:14 AM 2019/5/21
 */
public class Singleton {

    private static final Singleton singleton = new Singleton();

    private Singleton(){

    }

    public static Singleton getSingleton() {

        return singleton;
    }
}
