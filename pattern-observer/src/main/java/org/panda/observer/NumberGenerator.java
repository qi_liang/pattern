package org.panda.observer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *  生成数值的抽象类
 */
public abstract class NumberGenerator {
    /**
     *  保持有NumberGenerator 的Observer们
     */
    private List<Observer> observers = new ArrayList();

    /**
     *  注册Observer
     * @param observer
     */
    public void addObserver(Observer observer){
        observers.add(observer);
    }

    /**
     * 删除Observer
     * @param observer
     */
    public void deleteObserver(Observer observer){
        observers.remove(observer);
    }

    public void notifyObservers(){
        Iterator<Observer> observerIterator = observers.iterator();
        while (observerIterator.hasNext()){
            Observer observer = observerIterator.next();
            observer.update(this);
        }
    }

    /**
     * 获取数值
     * @return
     */
    abstract int getNumber();
    /**
     *  生成数值
     */
    abstract void excute();
}
