package org.panda.observer;

/**
 *  观察者接口
 */
public interface Observer {

    void update(NumberGenerator generator);
}
