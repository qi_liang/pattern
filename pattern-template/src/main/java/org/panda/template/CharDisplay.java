package org.panda.template;

public class CharDisplay extends AbstractDisplay {

    private  char ch;

    public CharDisplay(char ch){
        this.ch = ch;

    }

    @Override
    void open() {
        System.out.printf("<<");
    }

    @Override
    void print() {
        System.out.printf(ch+"");
    }

    @Override
    void close() {
        System.out.println(">>");
    }

}
