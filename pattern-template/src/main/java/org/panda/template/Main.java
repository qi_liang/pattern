package org.panda.template;

public class Main {

    public static void main(String[] args) {

        AbstractDisplay abstractDisplay = new CharDisplay('b');

        abstractDisplay.display();

        abstractDisplay = new StringDisplay("hello world");

        abstractDisplay.display();
    }
}
