package org.panda.template;

public class StringDisplay extends AbstractDisplay {

    private String str;

    private int width;

    public StringDisplay(String str) {
        this.str = str;
        this.width = str.getBytes().length;
    }

    @Override
    void open() {
        printLin();
    }

    @Override
    void print() {
        System.out.println("|"+str+"|");
    }

    @Override
    void close() {
        printLin();
    }

    private void printLin(){
        System.out.print("+");
        for (int i=0;i<width;i++){
            System.out.print("-");
        }
        System.out.println("+");
    }
}
