package org.panda.template;

/**
 * 模版方法
 * @author qi
 */
public abstract class AbstractDisplay {
    /**
     *  打开
     */
    abstract void open();

    /**
     *  打印
     */
    abstract void print();

    /**
     * 关闭
     */
    abstract void close();

    /**
     * 显示
     */
    final void display(){
        open();
        for (int i=0;i<5;i++){
            print();
        }
        close();
    }

}
