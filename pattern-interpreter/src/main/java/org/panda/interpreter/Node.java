package org.panda.interpreter;

/**
 * 节点
 * @author qi
 */
public interface Node {

    /**
     * 解析内容
     * @param context （内容）
     * @throws ParseException (解析异常)
     */
    void parse(Context context)throws ParseException;
}
