package org.panda.interpreter;

/**
 * 解析异常
 * @author qi
 */
public class ParseException extends Exception {

    public ParseException(String message) {
        super(message);
    }
}
