package org.panda.interpreter;

import java.util.ArrayList;
import java.util.List;

/**
 *  命令列表
 * @author qi
 */
public class CommandListNode implements Node {

    private List list = new ArrayList();

    @Override
    public void parse(Context context) throws ParseException {

        while (true){
            if (context.currentToken() == null){
                throw new ParseException("Missing 'end'");
            }else if ("end".equals(context.currentToken())){
                context.skipToken("end");
                break;
            }else {
                Node commandNode = new CommandNode();
                commandNode.parse(context);
                list.add(commandNode);
            }
        }
    }

    @Override
    public String toString() {
        return list.toString();
    }
}
