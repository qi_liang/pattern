package org.panda.interpreter;

import java.util.StringTokenizer;

/**
 * 内容
 * @author qi
 */
public class Context {
    /**
     * 用于分隔字符串
     */
    private StringTokenizer tokenizer;
    /**
     * 当前token
     */
    private String currentToken;

    public Context(String text){
        this.tokenizer = new StringTokenizer(text);
        nextToken();
    }

    public String nextToken(){
        if (tokenizer.hasMoreTokens()){
            currentToken = tokenizer.nextToken();
        }else {
            currentToken = null;
        }
        return currentToken;
    }

    public String currentToken(){
        return currentToken;
    }

    public void skipToken(String token)throws ParseException {
        if (!token.equals(currentToken)){
            throw new ParseException("Warning: "+token+"is expected,but "+currentToken+" is found");
        }
        nextToken();
    }

    public int currentNumber()throws ParseException {
        int number = 0;
        number = Integer.parseInt(currentToken);
        return number;
    }

}
