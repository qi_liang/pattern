package org.panda.interpreter;

public class CommandNode implements Node {
    /**
     * 命令-repeat
     */
    public static final String COMMAND_REPEAT = "repeat";

    private Node node;

    @Override
    public void parse(Context context) throws ParseException {
        if (COMMAND_REPEAT.equals(context.currentToken())){
            node = new RepeatCommandNode();
            node.parse(context);
        }else {
            node = new PrimitiveCommandNode();
            node.parse(context);
        }

    }

    @Override
    public String toString() {
        return node.toString();
    }
}
