package org.panda.interpreter;

/**
 *
 * @author qi
 */
public class RepeatCommandNode implements Node {

    private int number;
    /**
     * 命令列表
     */
    private Node commandListNode;

    @Override
    public void parse(Context context) throws ParseException {
        context.skipToken("repeat");
        number = context.currentNumber();
        context.nextToken();
        commandListNode = new CommandListNode();
        commandListNode.parse(context);
    }

    @Override
    public String toString() {
        return "[repeat "+number+" "+commandListNode+"]";
    }
}
