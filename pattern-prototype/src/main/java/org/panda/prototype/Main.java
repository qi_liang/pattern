package org.panda.prototype;

import org.panda.prototype.framework.Manager;
import org.panda.prototype.framework.Product;

/**
 * @Author: qi
 * @Description:
 * @Date: Create in 6:29 PM 2019/5/25
 */
public class Main {

    public static void main(String[] args) {
        //准备
        Manager manager = new Manager();
        UnderlinePen underlinePen = new UnderlinePen('~');
        MessageBox mBox = new MessageBox('*');
        MessageBox sBox = new MessageBox('/');
        manager.register("strong message",underlinePen);
        manager.register("warning box",mBox);
        manager.register("slash box",sBox);

        //生成
        Product p1 = manager.create("strong message");
        p1.use("hello world!");
        Product p2 = manager.create("warning box");
        p2.use("Hello world!");
        Product p3 = manager.create("slash box");
        p3.use("hello world!");

    }
}
