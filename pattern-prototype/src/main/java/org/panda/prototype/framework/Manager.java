package org.panda.prototype.framework;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: qi
 * @Description:原型管理类(Manager没有写明具体类名,通过接口Product作为桥梁解耦其他实例原型)
 * @Date: Create in 6:10 PM 2019/5/25
 */
public class Manager {
   /**
    * 保存名字和实例对应关系
    **/
   private Map<String,Product> showcase = new HashMap<>();

    /**
     *  注册名字和实例关系
     * @param name
     * @param proto
     */
   public void register(String name,Product proto){
       showcase.put(name,proto);
   }

   public Product create(String protoname){
       Product p = showcase.get(protoname);
       return p.createClone();
   }
}
