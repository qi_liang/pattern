package org.panda.prototype.framework;

/**
 * @Author: qi
 * @Description:原型接口
 * @Date: Create in 6:09 PM 2019/5/25
 */
public interface Product extends Cloneable {

    /**
     *  "使用"方法，具体实现交由子类实现
     * @param s
     */
    void use(String s);

    /**
     *  用于复制实例
     * @return
     */
    Product createClone();

}
