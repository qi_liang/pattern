package org.panda.entity;

import java.util.Date;

/**
 * @Author: qi
 * @Description:
 * @Date: Create in 6:44 PM 2019/5/25
 */
public class User {

    private Long id;

    private String name;

    private Date createTime;

    public User() {
    }

    public User(Long id, String name, Date createTime) {
        this.id = id;
        this.name = name;
        this.createTime = createTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
