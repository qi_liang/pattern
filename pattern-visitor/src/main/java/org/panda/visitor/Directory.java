package org.panda.visitor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @Author: qi
 * @Description:
 * @Date: Create in 5:39 AM 2019/6/9
 */
public class Directory extends Entry implements Element {

    private String name;

    private List<Entry> dirArray ;

    public Directory(String name) {
        this.name = name;
        this.dirArray = new ArrayList<>();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getSize() {
        int size = 0;
        Iterator<Entry> it = dirArray.iterator();
        while (it.hasNext()){
            Entry entry = it.next();
            size+=entry.getSize();
        }
        return size;
    }

    @Override
    public Entry add(Entry entry) throws FileTreatmentException {
        dirArray.add(entry);
        return entry;
    }

    @Override
    public Iterator<Entry> iterator() throws FileTreatmentException {
        return dirArray.iterator();
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
