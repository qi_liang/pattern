package org.panda.visitor;

/**
 * @Author: qi
 * @Description:
 * @Date: Create in 12:49 AM 2019/6/9
 */
public class FileTreatmentException extends RuntimeException {
    public FileTreatmentException(){

    }
    public FileTreatmentException(String msg){
        super(msg);
    }
}
