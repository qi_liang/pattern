package org.panda.visitor;

/**
 * @Author: qi
 * @Description:文件夹类
 * @Date: Create in 5:37 AM 2019/6/9
 */
public class File extends Entry {

    private String name;

    private int size;

    public File(String name, int size) {
        this.name = name;
        this.size = size;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
