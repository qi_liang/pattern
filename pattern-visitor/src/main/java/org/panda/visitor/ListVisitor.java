package org.panda.visitor;

import java.util.Iterator;

/**
 * @Author: qi
 * @Description:
 * @Date: Create in 5:48 AM 2019/6/9
 */
public class ListVisitor extends Visitor {

    private String currentDir = "";

    @Override
    public void visit(File file) {
        System.out.println(currentDir+"/"+file);
    }

    @Override
    public void visit(Directory directory) {
        System.out.println(currentDir+"/"+directory);
        String saveDir = currentDir;
        currentDir = currentDir+ "/" +directory.getName();
        Iterator<Entry> iterator = directory.iterator();
        while (iterator.hasNext()){
            Entry entry = iterator.next();
            entry.accept(this);
        }
        currentDir = saveDir;
    }
}
