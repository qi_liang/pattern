package org.panda.visitor;


/**
 * @Author: qi
 * @Description:访问者抽象类
 * @Date: Create in 12:45 AM 2019/6/9
 */
public abstract class Visitor {

    public abstract void visit(File file);

    public abstract void visit(Directory directory);
}
