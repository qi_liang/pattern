package org.panda.visitor;

import java.util.Iterator;

/**
 * @Author: qi
 * @Description:
 * @Date: Create in 12:47 AM 2019/6/9
 */
public abstract class Entry implements Element {

    /**
     * 获取名字
     * @return
     */
    public abstract String getName();

    /**
     * 获取大小
     * @return
     */
    public abstract int getSize();

    /**
     * 新增目录条目
     * @param entry
     * @return
     * @throws FileTreatmentException
     */
    public Entry add(Entry entry)throws FileTreatmentException{
        throw new FileTreatmentException();
    }

    public Iterator<Entry> iterator()throws FileTreatmentException{
        throw new FileTreatmentException();
    }

    @Override
    public String toString() {
        return getName()+"("+getSize()+")";
    }
}

