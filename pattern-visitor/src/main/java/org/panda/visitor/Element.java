package org.panda.visitor;

/**
 * @Author: qi
 * @Description: 表示接收访问者的访问接口
 * @Date: Create in 12:46 AM 2019/6/9
 */
public interface Element {
    /**
     *  接收方法
     * @param visitor（访问者）
     */
    void accept(Visitor visitor);
}
