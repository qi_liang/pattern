package org.panda;


import org.junit.Test;
import org.panda.iterator.Book;
import org.panda.iterator.BookShelf;
import org.panda.iterator.Iterator;

/**
 * 迭代器模式-测试类
 * @author qi
 */
public class AppTest {

    @Test
    public void test(){
        BookShelf bookShelf = new BookShelf();
        bookShelf.append(new Book("java 编程思想"));
        bookShelf.append(new Book("深入理解java虚拟机"));
        bookShelf.append(new Book("ftp权威指南"));
        bookShelf.append(new Book("http权威指南"));
        bookShelf.append(new Book("linux 鸟哥私房菜"));
        bookShelf.append(new Book("Head First Java"));
        bookShelf.append(new Book("Java 核心技术卷1+卷2"));
        Iterator<Book> iterator = bookShelf.iterator();
        while (iterator.hashNext()){
            Book book =  iterator.next();
            System.out.println(book.getName());
        }
    }
}
