package org.panda.iterator;

/**
 * 书架迭代器
 * @author qi
 */
public class BookShelfIterator implements Iterator {
    /**
     * 书架
     */
    private BookShelf bookShelf;
    /**
     * 索引
     */
    private int index;

    public BookShelfIterator(BookShelf bookShelf) {
        this.bookShelf = bookShelf;
        this.index = 0;
    }

    @Override
    public boolean hashNext() {

        return index<bookShelf.getLength();
    }

    @Override
    public Object next() {
        Book book = bookShelf.get(index);
        index++;
        return book;
    }

}
