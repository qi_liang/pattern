package org.panda.iterator;

/**
 *  集合类接口
 * @author qi
 */
public interface Aggregate {
    /**
     *  获取迭代器
     * @return
     */
    Iterator  iterator();
}
