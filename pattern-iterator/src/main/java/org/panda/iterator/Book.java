package org.panda.iterator;

/**
 *  书籍
 * @author qi
 */
public class Book {
    /**
     * 书名称
     */
    private String name;

    public Book() {
    }

    public Book(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
