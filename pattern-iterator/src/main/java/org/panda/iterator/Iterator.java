package org.panda.iterator;

/**
 *  接口-迭代器
 * @author qi
 * @param <T>
 */
public interface Iterator<T> {
    /**
     * 判断是否有下一元素
     * @return
     */
    boolean hashNext();

    /**
     * 获取元素
     * @return
     */
    T next();
}
