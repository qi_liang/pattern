package org.panda.iterator;

import java.util.ArrayList;
import java.util.List;

/**
 * 书的集合类-书架
 * @author qi
 */
public class BookShelf implements Aggregate {
    /**
     * 书籍集合
     */
    private List<Book>  bookList;
    /**
     * 集合索引
     */
    private int last = 0;

    public BookShelf(){

        bookList = new ArrayList<>();
    }

    public Book get(int index){
        return bookList.get(index);
    }

    public void remove(Book book){
        bookList.remove(book);
        this.last--;
    }

    public void remove(int index){
        bookList.remove(index);
        this.last--;
    }

    public void append(Book book){

        bookList.add(book);
        this.last++;
    }

    public int getLength(){
        return last;
    }

    @Override
    public Iterator iterator() {
        return new BookShelfIterator(this);
    }
}
