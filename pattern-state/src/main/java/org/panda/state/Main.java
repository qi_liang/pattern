package org.panda.state;

public class Main {

    public static void main(String[] args) {
        SafeFrame safeFrame = new SafeFrame("State Sample");
        while (true){
            for (int hour=0;hour<24;hour++){
                //设置时间
                safeFrame.setClock(hour);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
