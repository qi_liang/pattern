package org.panda.state;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SafeFrame extends Frame implements ActionListener,Context {
    //显示当前时间
    private TextField textClock = new TextField(60);
    //显示报警中心的记录
    private TextArea textScreen = new TextArea(10,60);
    //使用金库按钮
    private Button buttonUse = new Button("使用金库");
    //按下警铃
    private Button buttonAlarm = new Button("按下警铃");
    //正常通话按钮
    private Button buttonPhone = new Button("正常通话");
    //结束按钮
    private Button buttonExit = new Button("结束");

    private State state = DayState.getInstance();

    public SafeFrame(String title) throws HeadlessException {
        super(title);
        setBackground(Color.lightGray);
        setLayout(new BorderLayout());
        add(textClock,BorderLayout.NORTH);
        textClock.setEditable(false);
        add(textScreen,BorderLayout.CENTER);
        textScreen.setEnabled(false);

        Panel panel = new Panel();
        panel.add(buttonUse);
        panel.add(buttonAlarm);
        panel.add(buttonPhone);
        panel.add(buttonExit);

        add(panel,BorderLayout.SOUTH);

        pack();

        setVisible(true);

        buttonUse.addActionListener(this);
        buttonAlarm.addActionListener(this);
        buttonPhone.addActionListener(this);
        buttonExit.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println(e.toString());
        if (e.getSource()==buttonUse){
            state.doUse(this);
        }else if (e.getSource()== buttonAlarm){
            state.doAlarm(this);
        }else if (e.getSource()==buttonPhone){
            state.doPhone(this);
        }else if(e.getSource()==buttonExit){
            System.exit(0);
        }else {
            System.out.println("?");
        }
    }

    @Override
    public void setClock(int hour) {
        String colckstring = "现在时间是";
        if (hour<10){
            colckstring +="0"+hour+":00";
        }else{
            colckstring +=hour+":00";
        }
        System.out.println(colckstring);
        textClock.setText(colckstring);
        state.doClock(this,hour);
    }

    @Override
    public void changeState(State state) {
        System.out.println("从"+this.state+"变更为"+state+"状态。");
        this.state = state;
    }

    @Override
    public void callSecurityCenter(String msg) {
        textScreen.append("call:"+msg+"\n");
    }

    @Override
    public void recordLog(String msg) {
        textScreen.append("recordLog...."+msg);
    }
}
