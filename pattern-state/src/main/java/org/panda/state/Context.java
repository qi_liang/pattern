package org.panda.state;

public interface Context {
    //设置时间
    void setClock(int hour);
    //改变状态
    void changeState(State state);
    //联系报警中心
    void callSecurityCenter(String msg);
    //在报警中心留下记录
    void recordLog(String msg);
}
