package org.panda.mediator;

/**
 * @Author: qi
 * @Description:表示向仲裁者进行包的的组员的接口
 * @Date: Create in 12:24 AM 2019/6/21
 */
public interface Colleague {

    /**
     *  告知组员，"我是仲裁者，有事请报告我"
     * @param mediator
     */
    void setMediator(Mediator mediator);

    /**
     * 告知组员仲裁者所下达的指示
     * @param enabled true:启用 false:禁用
     */
    void setColleagueEnabled(boolean enabled);
}
