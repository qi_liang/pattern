package org.panda.mediator;

/**
 * @Author: qi
 * @Description: 仲裁者接口
 * @Date: Create in 12:15 AM 2019/6/21
 */
public interface Mediator {

    /**
     *  生成Mediator要管理的组员
     */
    void createColleagues();

    /**
     *  让组员可以向仲裁者进行报告
     */
    void colleagueChanged();
}
