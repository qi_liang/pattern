package org.panda.mediator;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @Author: qi
 * @Description:仲裁者具体实现类
 * @Date: Create in 12:46 AM 2019/6/21
 */
public class LoginFram extends Frame implements ActionListener,Mediator {
    //游客勾选框
    private ColleagueCheckbox checkGuest;
    //登录勾选框
    private ColleagueCheckbox checkLogin;
    //用户名输入框
    private ColleagueTextField textUser;
    //用户密码输入框
    private ColleagueTextField textPass;
    //点击OK
    private ColleagueButton buttonOk;
    //点击取消
    private ColleagueButton buttonCancel;


    public LoginFram(String title) throws HeadlessException {
        super(title);
        //设置背景颜色
        setBackground(Color.lightGray);
        //使用布局管理器生成4X2窗格
        setLayout(new GridLayout(4,2));
        //生成各个组件
        createColleagues();
        add(checkGuest);
        add(checkLogin);
        add(new Label("Username:"));
        add(textUser);
        add(new Label("Password:"));
        add(textPass);
        add(buttonOk);
        add(buttonCancel);
        //设置初始化状态
        colleagueChanged();
        //显示
        pack();
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println(e.toString());
        System.exit(0);
    }

    @Override
    public void createColleagues() {
        //生成
        CheckboxGroup checkboxGroup = new CheckboxGroup();
        checkGuest = new ColleagueCheckbox("Guest",checkboxGroup,true);
        checkLogin = new ColleagueCheckbox("Login",checkboxGroup,false);
        textUser = new ColleagueTextField("",10);
        textPass = new ColleagueTextField("",10);
        textPass.setEchoChar('*');
        buttonOk = new ColleagueButton("OK");
        buttonCancel = new ColleagueButton("Cancel");
        //设置Mediator
        checkGuest.setMediator(this);
        checkLogin.setMediator(this);
        textUser.setMediator(this);
        textPass.setMediator(this);
        buttonOk.setMediator(this);
        buttonCancel.setMediator(this);
        //设置Listener
        checkGuest.addItemListener(checkGuest);
        checkLogin.addItemListener(checkLogin);
        textUser.addTextListener(textUser);
        textPass.addTextListener(textPass);
        buttonOk.addActionListener(this);
        buttonCancel.addActionListener(this);

    }

    @Override
    public void colleagueChanged() {
        if (checkGuest.getState()){
            textUser.setColleagueEnabled(false);
            textPass.setColleagueEnabled(false);
            buttonOk.setColleagueEnabled(true);
        }else {
            textUser.setColleagueEnabled(true);
            userPassChanged();
        }
    }

    //当textuser或者textPass文本输入框中的文字发生变化时
    //判断 各colleage的状态
    private void userPassChanged(){

        if (textUser.getText().length()>0){
            textPass.setColleagueEnabled(true);
            if (textPass.getText().length()>0){
                buttonOk.setColleagueEnabled(true);
            }else {
                buttonOk.setColleagueEnabled(false);
            }
        }else{
            textPass.setColleagueEnabled(false);
            buttonOk.setColleagueEnabled(false);
        }
    }
}
