package org.panda.builder;

/**
 * @Author: qi
 * @Description:构造器
 * @Date: Create in 8:21 PM 2019/5/25
 */
public interface Builder {
    /**
     * 制造标题
     * @param title
     */
    void makeTitle(String title);

    /**
     * 制作内容
     * @param str
     */
    void makeString(String str);

    /**
     * 制作选项
     * @param items
     */
    void makeItems(String [] items);

    /**
     * 关闭方法
     */
    void close();

}
