package org.panda.builder;

/**
 * @Author: qi
 * @Description:
 * @Date: Create in 8:50 PM 2019/5/25
 */
public class Main {

    public static void main(String[] args) {
        if (args.length!=1){
            usage();
            System.exit(0);
        }
        if ("plain".equals(args[0])){
            TextBuilder textBuilder = new TextBuilder();
            Directory directory = new Directory(textBuilder);
            directory.construct();
            String result = textBuilder.getResult();
            System.out.println(result);
        }else if ("html".equals(args[0])){
            HTMLBuilder htmlBuilder = new HTMLBuilder();
            Directory directory = new Directory(htmlBuilder);
            directory.construct();;
            String filename= htmlBuilder.getResult();
            System.out.println(filename+"文档完成编写");
        }else {
            usage();
            System.exit(0);
        }
    }

    public static void usage(){
        System.out.println("Usage: java Main plain 编写纯文本文档");
        System.out.println("Usage: java Main html 编写HTML文档");
    }
}
