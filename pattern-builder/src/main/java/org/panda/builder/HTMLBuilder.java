package org.panda.builder;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @Author: qi
 * @Description:
 * @Date: Create in 8:36 PM 2019/5/25
 */
public class HTMLBuilder implements Builder {

    private String fileName;

    private PrintWriter writer;

    @Override
    public void makeTitle(String title) {
        fileName = title+".html";

        try {
            writer = new PrintWriter(new FileWriter(fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        writer.println("<html><head><title>"+title+"</title></head><body>");
        writer.println("<h1>"+title+"</h1>");
    }

    @Override
    public void makeString(String str) {
        writer.println("<p>"+str+"</p>");
    }

    @Override
    public void makeItems(String[] items) {
        writer.println("<ul>");
        for (int i=0;i<items.length;i++){
            writer.println("<li>"+items[i]+"</li>");
        }
        writer.println("</ul>");
    }

    public String getResult(){

        return fileName;
    }

    @Override
    public void close() {

    }

}
