package org.panda.facade.pagemaker;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * @Author: qi
 * @Description:
 * @Date: Create in 11:34 PM 2019/6/13
 */
public class DataBase {

    private DataBase(){

    }

    public static Properties getProperties(String dbName){
        String fileName = dbName+".txt";
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(fileName));
        } catch (IOException e) {
            System.out.println("Warning:"+fileName+" is not found.");
            e.printStackTrace();
        }
        return properties;
    }
}
