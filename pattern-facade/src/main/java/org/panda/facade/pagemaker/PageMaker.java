package org.panda.facade.pagemaker;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

/**
 * @Author: qi
 * @Description:
 * @Date: Create in 11:53 PM 2019/6/13
 */
public class PageMaker {

    private PageMaker(){

    }

    public static void makeWelcomePage(String mailaddr,String fileName){
    try {

        Properties mailProp = DataBase.getProperties("maildata");
        String userName = mailProp.getProperty(mailaddr);
        HtmlWriter writer = new HtmlWriter(new FileWriter(fileName));
        writer.title("Wecome to "+userName+"'s page!");
        writer.paragraph(userName+"欢迎来的"+userName+"的主页。");
        writer.paragraph("等着你的邮件哦");
        writer.mailto(mailaddr,userName);
        writer.close();
        System.out.println(fileName+ "is create for "+mailaddr+"("+userName+")");
    } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
