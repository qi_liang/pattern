package org.panda.command;

/**
 *  表示"命令"的接口
 * @author qi
 */
public interface Command {
    /**
     * 执行方法
     */
    void execute();
}
