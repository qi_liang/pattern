package org.panda.command;

/**
 * 表示 "绘制对象" 接口
 * @author qi
 */
public interface Drawable {
    /**
     *  绘制
     * @param x
     * @param y
     */
    void draw(int x,int y);
}
