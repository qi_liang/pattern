package org.panda.command;

import java.util.Iterator;
import java.util.Stack;

/**
 *  命令集合
 * @author qi
 */
public class MacroCommand implements Command {
    /**
     *  命令集合
     */
    private Stack<Command> commands = new Stack();

    @Override
    public void execute() {
        Iterator<Command> commandIterator = commands.iterator();
        while (commandIterator.hasNext()){
            commandIterator.next().execute();
        }
    }

    /**
     *  添加命令
     * @param cmd
     */
    public void append(Command cmd){
        if (cmd!=this){
            commands.push(cmd);
        }
    }

    /**
     * 删除最后一条命令
     */
    public void undo(){
        if (!commands.empty()){
            commands.pop();
        }
    }

    /**
     * 删除所有命令
     */
    public void clean(){
        commands.clear();
    }
}
