# 适配器模式

模型：结构模型

思想：
* 适配器模式有两种实现方式：继承和委托
* 适配器模式把一个类的接口变换成客户端所期待的另一种接口，从而使原本因接口不匹配而无法在一起工作的两个类能够在一起工作

好处：

* 对经过充分测试的现有类，实现复用，减少代码。