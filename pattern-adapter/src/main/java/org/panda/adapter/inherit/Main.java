package org.panda.adapter.inherit;


import org.panda.adapter.Print;

public class Main {

    public static void main(String[] args) {
        Print print = new BannerPrint("hello");
        print.printStrong();
        print.printWeak();
    }
}
