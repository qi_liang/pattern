package org.panda.adapter.inherit;

import org.panda.adapter.Banner;
import org.panda.adapter.Print;

/**
 * 适配器-实现方式-继承
 * 横幅打印
 * @author qi
 */
public class BannerPrint extends Banner implements Print {

    public BannerPrint(String string) {
        super(string);
    }

    @Override
    public void printWeak() {
        showWithParen();
    }

    @Override
    public void printStrong() {
        showWithAster();
    }
}
