package org.panda.adapter;

/**
 * 横幅
 * @author qi
 */
public class Banner {
    /**
     * 内容
     */
    private String string;

    public Banner(String string) {
        this.string = string;
    }

    public void showWithParen(){
        System.out.println("("+string+")");
    }

    public void showWithAster(){
        System.out.println("*"+string+"*");
    }

}
