package org.panda.adapter;

/**
 * 抽象类-打印
 * @author qi
 */
public interface Print {
    /**
     * 打印括号
     */
    void printWeak();

    /**
     * 打印点
     */
    void printStrong();
}
