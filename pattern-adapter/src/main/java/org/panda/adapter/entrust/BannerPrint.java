package org.panda.adapter.entrust;


import org.panda.adapter.Banner;
import org.panda.adapter.Print;

/**
 *  适配器-实现方式-代理
 *  横幅打印
 * @author qi
 */
public class BannerPrint  implements Print {

    private Banner banner;

    public BannerPrint(String string) {

        this.banner = new Banner(string);
    }

    @Override
    public void printWeak() {
        banner.showWithParen();
    }

    @Override
    public void printStrong() {
        banner.showWithAster();
    }
}
