package org.panda.responsibility;

/**
 * @Author: qi
 * @Description:只解决指定编号的问题
 * @Date: Create in 6:25 AM 2019/6/10
 */
public class SpecialSupport extends Support {

    private int number;
    /**
     * 生成解决问题的实例
     *
     * @param name
     */
    public SpecialSupport(String name,int number) {
        super(name);
        this.number = number;
    }

    @Override
    protected boolean resolve(Trouble trouble) {
        if (trouble.getNumber()==number){
            return true;
        }else {
            return false;
        }
    }
}
