package org.panda.responsibility;

/**
 * @Author: qi
 * @Description:阶级编号小于limit值的问题
 * @Date: Create in 6:20 AM 2019/6/10
 */
public class LimitSupport extends Support {

    private int limit;

    /**
     * 生成解决问题的实例
     *
     * @param name
     */
    public LimitSupport(String name,int limit) {
        super(name);
        this.limit = limit;
    }

    @Override
    protected boolean resolve(Trouble trouble) {
        if (trouble.getNumber()<limit){
            return true;
        }else {
            return false;
        }
    }
}
