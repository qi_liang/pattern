package org.panda.responsibility;

/**
 * @Author: qi
 * @Description：解决问题的抽象类
 * @Date: Create in 7:01 AM 2019/6/9
 */
public abstract class Support {
    /**
     * 解决问题的实例的名字
     */
    private String name;
    /**
     * 要推卸给的对象
     */
    private Support next;

    /**
     * 生成解决问题的实例
     * @param name
     */
    public Support(String name) {
        this.name = name;
    }

    /**
     * 设置要推卸的对象
     * @param next
     */
    public Support setNext(Support next) {
        this.next = next;
        return next;
    }

    /**
     *  解决问题的步骤
     * @param trouble
     */
    public final void support(Trouble trouble){
        if (resolve(trouble)){
            done(trouble);
        }else  if (next!=null){
            next.support(trouble);
        }else {
            fanil(trouble);
        }
    }

    /**
     * 解决问题的方法
     * @param trouble
     * @return
     */
    protected abstract boolean resolve(Trouble trouble);

    /**
     * 解决
     * @param trouble
     */
    protected void done(Trouble trouble){
        System.out.println(trouble+" is resolved by "+this+".");
    }

    /**
     * 未解决
     * @param trouble
     */
    protected void fanil(Trouble trouble){
        System.out.println(trouble+" cannot be resolved.");
    }

    @Override
    public String toString() {
        return "【"+name+"】";
    }
}
