package org.panda.responsibility;

/**
 * @Author: qi
 * @Description:解决奇数编号的问题
 * @Date: Create in 6:22 AM 2019/6/10
 */
public class OddSupport extends Support {

    /**
     * 生成解决问题的实例
     *
     * @param name
     */
    public OddSupport(String name) {
        super(name);
    }

    @Override
    protected boolean resolve(Trouble trouble) {
        if (trouble.getNumber()%2==1){
            return true;
        }else {
            return false;
        }
    }
}
