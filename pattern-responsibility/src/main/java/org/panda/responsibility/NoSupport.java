package org.panda.responsibility;

/**
 * @Author: qi
 * @Description: 不解决问题的子类
 * @Date: Create in 6:18 AM 2019/6/10
 */
public class NoSupport  extends Support{
    /**
     * 生成解决问题的实例
     *
     * @param name
     */
    public NoSupport(String name) {
        super(name);
    }

    /**
     *  解决问题的办法
     * @param trouble 自己什么都不处理
     * @return
     */
    @Override
    protected boolean resolve(Trouble trouble) {
        return false;
    }
}
