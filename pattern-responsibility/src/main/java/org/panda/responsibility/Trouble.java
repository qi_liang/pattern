package org.panda.responsibility;

/**
 * @Author: qi
 * @Description:
 * @Date: Create in 6:03 AM 2019/6/10
 */
public class Trouble {

    /**
     * 问题编号
     */
    private int number;

    /**
     *  生成问题
     * @param number
     */
    public Trouble(int number) {
        this.number = number;
    }

    /**
     * 获取问题编号
     * @return
     */
    public int getNumber() {
        return number;
    }

    @Override
    public String toString() {
        return "【Trouble"+number+"】";
    }
}
