package org.panda.bridage;

/**
 * @Author: qi
 * @Description:
 * @Date: Create in 7:45 PM 2019/5/26
 */
public abstract class DisplayImpl {

    public abstract void rawOpen();

    public abstract void rawPrint();

    public abstract void rawClose();

}
