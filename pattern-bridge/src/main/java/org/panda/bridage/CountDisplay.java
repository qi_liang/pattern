package org.panda.bridage;

/**
 * @Author: qi
 * @Description:
 * @Date: Create in 7:47 PM 2019/5/26
 */
public class CountDisplay extends Display {

    public CountDisplay(DisplayImpl impl) {
        super(impl);
    }

    public void multiDisplay(int times){
        open();
        for (int i = 0;i < times; i++){
            print();
        }
        close();
    }
}
