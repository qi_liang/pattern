package org.panda.abstractFactory;

import org.panda.abstractFactory.factory.Factory;
import org.panda.abstractFactory.factory.Link;

/**
 * @Author: qi
 * @Description:
 * @Date: Create in 9:35 PM 2019/5/25
 */
public class Main {

    public static void main(String[] args) {
        if (args.length!=1){
            System.out.println("Usage: java Main class.name.of.ConcreteFactory");
            System.out.println("Example 1: java Main listFactory.ListFactory");
            System.out.println("Example 2: java Main tableFactory.TableFactory");
            System.exit(0);
        }
        Factory factory = Factory.getFactory(args[0]);

        Link people = factory.createLink("人民日报 ","http://www.people.com.cn/");
        Link gmw = factory.createLink("光明日报","http://www.gmw.cn/");

    }
}
