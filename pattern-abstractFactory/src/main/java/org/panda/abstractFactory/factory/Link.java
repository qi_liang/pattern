package org.panda.abstractFactory.factory;

/**
 * @Author: qi
 * @Description:HTML的超链接类(抽象类)
 * @Date: Create in 9:40 PM 2019/5/25
 */
public abstract class Link extends Item {
    /**
     * 超链接指向的地址
     */
    protected String url;

    public Link(String caption,String url) {
        super(caption);
        this.url = url;
    }
}
