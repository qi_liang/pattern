package org.panda.abstractFactory.factory;

/**
 * @Author: qi
 * @Description:抽象工厂
 * @Date: Create in 10:00 PM 2019/5/25
 */
public abstract class Factory {

    public static Factory getFactory(String className){
        Factory factory = null;
        try {
            factory = (Factory) Class.forName(className).newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return factory;
    }

    /**
     *  创建超链接类
     * @param caption
     * @param url
     * @return
     */
    public abstract Link createLink(String caption,String url);

    /**
     * 创建托盘抽象类
     * @param caption
     * @return
     */
    public abstract Tray createTary(String caption);

    /**
     * 创建抽象产品
     * @param title
     * @param author
     * @return
     */
    public abstract Page createPage(String title,String author);
}
