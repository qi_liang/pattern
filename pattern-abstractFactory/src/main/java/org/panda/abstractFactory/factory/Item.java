package org.panda.abstractFactory.factory;

/**
 * @Author: qi
 * @Description:零件抽象类
 * @Date: Create in 9:38 PM 2019/5/25
 */
public abstract class Item {
    /**
     *  标题
     */
    protected String caption;

    public Item(String caption) {
        this.caption = caption;
    }

    /**
     *  返回HTML文件内容(抽象类方法，需子类实现)
     * @return
     */
    abstract String makeHtml();

}
