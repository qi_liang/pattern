package org.panda.abstractFactory.factory;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: qi
 * @Description:Tary托盘类，包含多个Link和Tary类的容器
 * @Date: Create in 9:45 PM 2019/5/25
 */
public abstract class Tray extends Item {

    /**
     *  零件容器
     */
    protected List<Item> tray = new ArrayList();

    public Tray(String caption) {
        super(caption);
    }

    /**
     * 提交零件
     * @param item 零件抽象类
     */
    public void add(Item item){
        tray.add(item);
    }
}
