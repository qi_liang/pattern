package org.panda.abstractFactory.factory;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: qi
 * @Description:Html页面抽象类（抽象产品）
 * @Date: Create in 9:50 PM 2019/5/25
 */
public abstract class Page {
    /**
     * 页面标题
     */
    protected String title;
    /**
     * 页面作者
     */
    protected String author;
    /**
     * 页面内容(抽象类item容器)
     */
    protected List<Item> content = new ArrayList();

    public Page(String title, String author) {
        this.title = title;
        this.author = author;
    }

    public void add(Item item){
        content.add(item);
    }

    public void output(){
        String fileName = title+".html";
        try {
            Writer writer = new FileWriter(fileName);
            writer.close();
            System.out.println(fileName+"编写完成.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    abstract String makeHtml();

}
