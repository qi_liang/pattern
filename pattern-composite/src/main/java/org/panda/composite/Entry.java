package org.panda.composite;

/**
 * @Author: qi
 * @Description:抽象类，实现File类和Directory类的一致性
 * @Date: Create in 6:00 AM 2019/6/3
 */
public abstract class Entry {

    abstract String getName();

    abstract int getSize();

    public Entry add(Entry entry)throws FileTreatementException{
        throw new FileTreatementException();
    }

    public void printList(){
        printList("");
    }

    protected  abstract void printList(String prefix);

    @Override
    public String toString() {
        return getName()+"("+getSize()+")";
    }
}
