package org.panda.composite;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @Author: qi
 * @Description:复合物
 * @Date: Create in 6:02 AM 2019/6/3
 */
public class Directory extends Entry {

    private String name;

    private List<Entry> directory;

    public Directory(String name) {
        this.name = name;
        this.directory = new ArrayList<>();
    }

    @Override
    String getName() {
        return name;
    }

    @Override
    int getSize() {
        int size = 0;
        Iterator<Entry> it =directory.iterator();
        while (it.hasNext()){
            Entry entry = it.next();
            size += entry.getSize();
        }
        return size;
    }

    @Override
    protected void printList(String prefix) {
        System.out.println(prefix+"/"+this);
        Iterator<Entry> iterator = directory.iterator();
        while (iterator.hasNext()){
            Entry entry = iterator.next();
            entry.printList(prefix+"/"+name);
        }
    }

    @Override
    public Entry add(Entry entry) {
        directory.add(entry);
        return this;
    }
}
