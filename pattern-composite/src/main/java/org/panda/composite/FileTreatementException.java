package org.panda.composite;

/**
 * @Author: qi
 * @Description:向文件中添加内容时候发生的异常
 * @Date: Create in 6:02 AM 2019/6/3
 */
public class FileTreatementException extends RuntimeException{

    public FileTreatementException() {
    }

    public FileTreatementException(String msg){
        super(msg);
    }
}
