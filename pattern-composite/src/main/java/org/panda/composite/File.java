package org.panda.composite;

import org.panda.composite.Entry;

/**
 * @Author: qi
 * @Description:Lefa(树叶)
 * @Date: Create in 6:01 AM 2019/6/3
 */
public class File extends Entry {

    private String name;

    private int size;

    public File(String name, int size) {
        this.name = name;
        this.size = size;
    }

    @Override
    String getName() {
        return name;
    }

    @Override
    int getSize() {
        return size;
    }

    @Override
    protected void printList(String prefix) {
        System.out.println(prefix+"/"+this);
    }
}
