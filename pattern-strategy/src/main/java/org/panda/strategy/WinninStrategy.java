package org.panda.strategy;

import java.util.Random;

/**
 * @Author: qi
 * @Description:策略具体实现类
 * @Date: Create in 9:40 AM 2019/5/30
 */
public class WinninStrategy implements Strategy {

    private Random random;

    private boolean won =false;

    private  Hand prevHand;

    public WinninStrategy(int seed) {
        random = new Random(seed);
    }

    @Override
    public Hand nextHand() {
        if (!won){
            prevHand = Hand.getHand(random.nextInt(3));
        }
        return prevHand;
    }

    @Override
    public void study(boolean win) {
        won = win;
    }
}
