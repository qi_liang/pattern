package org.panda.strategy;

/**
 * @Author: qi
 * @Description:策略抽象接口(策略角色)
 * @Date: Create in 9:35 AM 2019/5/30
 */
public interface Strategy {

    Hand nextHand();

    void study(boolean win);

}
