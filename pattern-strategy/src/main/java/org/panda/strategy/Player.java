package org.panda.strategy;

/**
 * @Author: qi
 * @Description:上下文(负责使用Strategy角色)
 * @Date: Create in 10:15 AM 2019/5/30
 */
public class Player {

    private String name;

    private Strategy strategy;

    private int winCount;

    private int loseCount;

    private int gameCount;

    /**
     *  赋予姓名和策略
     * @param name
     * @param strategy
     */
    public Player(String name, Strategy strategy) {
        this.name = name;
        this.strategy = strategy;
    }

    /**
     *  策略决定下一局要出的手势
     * @return
     */
    public Hand nextHand(){

        return strategy.nextHand();
    }

    /**
     *  胜利执行方法
     */
    public void win(){
        strategy.study(true);
        winCount++;
        gameCount++;
    }

    /**
     *  失败执行方法
     */
    public void close(){
        strategy.study(false);
        loseCount++;
        gameCount++;
    }

    /**
     *  打平执行方法
     */
    public void even(){
        gameCount++;
    }


    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", strategy=" + strategy +
                ", winCount=" + winCount +
                ", loseCount=" + loseCount +
                ", gameCount=" + gameCount +
                '}';
    }
}
