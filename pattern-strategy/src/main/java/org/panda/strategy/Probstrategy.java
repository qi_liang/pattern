package org.panda.strategy;

import java.util.Random;

/**
 * @Author: qi
 * @Description:策略具体实现类（具体策略角色）
 * @Date: Create in 9:51 AM 2019/5/30
 */
public class Probstrategy implements Strategy {

    private Random random;
    private int prevHandValue = 0;
    private int currentHandValue = 0;
    private int [][] history = {
            {1,1,1},
            {1,1,1},
            {1,1,1}
    };

    public Probstrategy(int seed) {
        random = new Random(seed);
    }

    @Override
    public Hand nextHand() {
        int bet =random.nextInt(getSum(currentHandValue));
        int handvalue = 0;
        if (bet<history[currentHandValue][0]){
            handvalue = 0;
        }else if (bet<history[currentHandValue][0]+history[currentHandValue][1]){
            handvalue = 1;
        }else {
            handvalue = 2;
        }
        prevHandValue = currentHandValue;
        currentHandValue = handvalue;
        return Hand.getHand(handvalue);
    }

    private int getSum(int hv){
        int sum = 0;
        for (int i=0;i<3;i++){
            sum+= history[hv][i];
        }
        return sum;
    }

    @Override
    public void study(boolean win) {

        if (win){
            history[prevHandValue][currentHandValue]++;
        }else {
            history[prevHandValue][(currentHandValue+1)%3]++;
            history[prevHandValue][(currentHandValue+2)%3]++;
        }
    }
}
